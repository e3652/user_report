# -*- coding: utf-8 -*-
from odoo import fields, models, api


class ReportOfSalesByDate(models.TransientModel):
    _name = 'user_report.report_of_sales_by_date'
    _description = 'Modelo que crea un wizard para generar un reporte de ventas por fecha'

    initial_date = fields.Date(
        string='Fecha Inicial'
    )
    end_date = fields.Date(
        string='Fecha Final'
    )
